

## Installation

For calling TimesFM, We have two environment files. Inside `timesfm`, for
GPU installation (assuming CUDA 12 has been setup), you can create a conda
environment `tfm_env` from the base folder through:

```
conda env create --file=environment.yml
```

For a CPU setup please use,

```
conda env create --file=environment_cpu.yml
```
to create the environment instead.

Follow by

```
conda activate tfm_env
pip install -e .
```
to install the package.



## Run

python t.py 

> forecast_plot.png

